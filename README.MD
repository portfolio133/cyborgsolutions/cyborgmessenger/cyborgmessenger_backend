# CyborgMessenger Backend

## Description
The "CyborgMessenger" project is an advanced internet messenger designed using various technologies to provide a comprehensive and intuitive platform for user communication. 
Below, we outline the key aspects of this project.

This backend code represents a crucial part of the larger CyborgMessenger project.  
Detailed documentation for the entire project can be found in the [repository](https://gitlab.com/portfolio133/cyborgsolutions/cyborgmessenger/cyborgmessenger_docs).


## Contributors
The project is created as part of the virtual company CyborgSolutions, where we aggregate the best IT specialists.  
Below is a list of contributors who have played a key role in advancing the application with short description:
1. [Andrzej Połetek](https://gitlab.poletek.net) - As a CEO, techlead, and programmer
2. [Ewa Kucała](https://github.com/Ewa-Anna) - Backend Developer

## Development environment
Running development environment is easy because we are using docker-compose and docker.  
`docker-compose build` - Will build all necesary images for run containers  
`docker-compose up`  - Running all of necesary containers