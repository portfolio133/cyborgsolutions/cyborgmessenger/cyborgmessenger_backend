from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


GENDER_CHOICES = [
    ("M", "Male"),
    ("F", "Female"),
    ("N", "Prefer not to say"),
]


class Profile(models.Model):
    # Extending Django's built-in user model
    # consider replacing with a custom user model.
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    # For development phase, for prod - AWS - ImageField
    avatar_url = models.URLField(blank=True)
    bio = models.TextField(blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(
        max_length=1, choices=GENDER_CHOICES, blank=True, null=True
    )

    # Language preference, theme settings, etc.

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created"]
        indexes = [models.Index(fields=["user"])]

    def __str__(self):
        return f"{self.user.username}'s profile"

    def full_name(self):
        return f"{self.user.first_name} {self.user.last_name}"
