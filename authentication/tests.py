from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse


class UserRegistrationTests(TestCase):
    def test_user_registration(self):
        c = Client()

        request = (reverse("authentication:rest_register"), {
            "username": "user1",
            "password1": "user1_Password",
            "password2": "user1_Password"
        })

        response = c.post(*request)
        self.assertEquals(response.status_code,204)

        # expect to fail second registration of the same user
        response = c.post(*request)
        self.assertEquals(response.status_code, 400)


class UserBasicTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user(username='user1', password='user1_Password')
        cls.user1.password = 'user1_Password'
        cls.user2 = User.objects.create_user(username='user2', password='user2_Password')
        cls.user2.password = 'user2_Password'

    @staticmethod
    def serialize_user(user: User) -> dict:
        return {
            "username": user.username,
            "password": user.password
        }

    @staticmethod
    def get_header(key: str) -> dict:
        return {
            "Authorization": f"Token {key}"
        }

    def test_user_login_successful(self):
        c = Client()

        # Login first user
        response = c.post(reverse("authentication:rest_login"), self.serialize_user(self.user1))
        self.assertEquals(response.status_code, 200)
        key1 = response.json()["key"]

        # Login second user
        response = c.post(reverse("authentication:rest_login"), self.serialize_user(self.user2))
        self.assertEquals(response.status_code, 200)
        key2 = response.json()["key"]

        # Using first user token get user details
        response = c.get(reverse("authentication:rest_user_details"), headers=self.get_header(key1))
        self.assertEquals(response.status_code, 200)
        user = response.json()["username"]
        self.assertEquals(user, self.user1.username)

        # Using second user token get user details
        response = c.get(reverse("authentication:rest_user_details"), headers=self.get_header(key2))
        self.assertEquals(response.status_code, 200)
        user = response.json()["username"]
        self.assertEquals(user, self.user2.username)

        # Using nonexisting token get user details
        response = c.get(reverse("authentication:rest_user_details"), headers=self.get_header(key2[:-1]))
        self.assertEquals(response.status_code, 401)

        # Logout first user and check if the can get user details
        response = c.post(reverse("authentication:rest_logout"), headers=self.get_header(key1))
        self.assertEquals(response.status_code, 200)
        response = c.get(reverse("authentication:rest_user_details"), headers=self.get_header(key1))
        self.assertEquals(response.status_code, 401)

        # Check second user is still logged-in
        response = c.get(reverse("authentication:rest_user_details"), headers=self.get_header(key2))
        self.assertEquals(response.status_code, 200)
        user = response.json()["username"]
        self.assertEquals(user, self.user2.username)

    def test_user_login_unsuccessful(self):
        c = Client()
        user = self.serialize_user(self.user1)
        user["password"] = "xxx"
        response = c.post(reverse("authentication:rest_login"), user)
        self.assertEquals(response.status_code, 400)